import jwt from 'jsonwebtoken';
import { AuthenticationError } from 'apollo-server-express';

class Auth {
    accessToken(res, req) {
        const accessToken = req.cookies.access_token;
        const refreshToken = req.cookies.refresh_token;

        if (!accessToken) return;

        try {
            const user = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
            delete user.iat;
            delete user.exp;
            delete user.nbf;
            delete user.jti;

            return user;
        } catch (err) {
            try {
                const user = jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET);
                delete user.iat;
                delete user.exp;
                delete user.nbf;
                delete user.jti;

                const token = this.createAccessToken(user);

                res.cookie('access_token', token, {
                    httpOnly: true,
                    secure: process.env.NODE_ENV === 'production',
                    maxAge: 1000 * 60 * 10,
                });

                return user;
            } catch (error) {
                res.clearCookie('access_token');
                res.clearCookie('refresh_token');
                throw new AuthenticationError('You are not authorizated.');
            }
        }
    }

    createAccessToken({ id, email }) {
        const token = jwt.sign({ id, email }, process.env.ACCESS_TOKEN_SECRET, {
            expiresIn: '10s',
        });
        return token;
    }

    createRefreshToken(accessToken) {
        try {
            const payload = jwt.verify(accessToken, process.env.ACCESS_TOKEN_SECRET);
            delete payload.iat;
            delete payload.exp;
            delete payload.nbf;
            delete payload.jti;
            return jwt.sign(payload, process.env.REFRESH_TOKEN_SECRET, { expiresIn: '15s' });
        } catch (err) {
            throw new AuthenticationError('You are not authorizated.');
        }
    }
}

export default new Auth();
