import Sequelize from 'sequelize';
import config from '@server.config';
import Logger from '@server.logger';

const dbEnv = config.database[config.environment];

const sequelize = new Sequelize(dbEnv.database, dbEnv.username, dbEnv.password, {
    host: dbEnv.host,
    dialect: dbEnv.dialect,
    logging: false,
    options: {
        operatorsAliases: Sequelize.Op,
    },
});

sequelize
    .authenticate()
    .then(() => {
        Logger.info(`Database connected.`);
    })
    .catch(err => {
        console.log(err);
        Logger.error(`Unable to connect to the database.`);
    });

export default sequelize;
