import webpack from 'webpack';
import WebpackDevServer from 'webpack-dev-server';
import express from 'express';
import { ApolloServer } from 'apollo-server-express';
import { resolve } from 'path';
import cookieParser from 'cookie-parser';
import webpackConfig from '@config.webpack.client';
import auth from '@server.services.auth';
import Logger from '@server.logger';
import config from '@server.config';
import models from '@server.models';
import schema from '@server.schemas';
import expressRouter from '@server.router';

class Setup {
    constructor() {
        this.serverApp = null;
        this.errorBoundary = {
            isError: false,
            error: null,
            code: null,
            text: null,
        };
    }

    setErrorBoundaryVariables(error, code, text) {
        this.errorBoundary = {
            isError: true,
            error,
            code,
            text,
        };
        Logger.warn('Something went wrong!!');
        Logger.error(`${code}: ${text}. Error: ${error}`);
    }

    createServerApp() {
        try {
            this.serverApp = express();
            this.serverApp.use(Logger.middleware);

            Logger.info(`Preparing install...`);
            Logger.info(`Environment : ${process.env.NODE_ENV}`);
            Logger.info(`ProcessID   : ${process.pid}`);

            return this.serverApp;
        } catch (err) {
            this.setErrorBoundaryVariables(err, 'S001', "Couldn't create an Express application.");
        }
    }

    setApplicationRouter() {
        Logger.info('Application router initalizing...');
        try {
            this.serverApp.use('/', expressRouter);
        } catch (err) {
            this.setErrorBoundaryVariables(err, 'SS003', "Couldn't reach server router.");
        }
    }

    setWebpackDevServer() {
        try {
            if (process.env.NODE_ENV === 'development') {
                Logger.info(`Setting webpack development server...`);
                const config = webpackConfig(process.env.NODE_ENV, {
                    mode: 'development',
                });
                const webpackServer = new WebpackDevServer(webpack(config), {
                    compress: true,
                    noInfo: true,
                    stats: 'errors-only',
                    hot: true,
                    contentBase: resolve(__dirname, '..', 'dist'),
                    historyApiFallback: {
                        index: 'index.html',
                    },
                });

                webpackServer.listen(config.devServer.port, config.devServer.host, err => {
                    if (err) {
                        console.log(err);
                    }

                    Logger.info(`WebpackDevServer created successfully...`);
                    Logger.info(`http://${config.devServer.host}:${config.devServer.port}`);
                });
            }
        } catch (err) {
            this.setErrorBoundaryVariables(err, 'S004', "Couldn't setup webpack.");
        }
    }

    setGraphqlServer() {
        const dbEnv = config.database[config.environment];
        Logger.info(`Setting graphql server...`);
        try {
            Logger.info(`Connecting database...`);
            Logger.info(`Database  : ${dbEnv.database}`);
            Logger.info(`Dialect   : ${dbEnv.dialect}`);
            Logger.info(`Host      : ${dbEnv.host}`);
            Logger.info(`Force     : ${dbEnv.force}`);

            this.serverApp.use(cookieParser());

            models.sequelize.sync({ force: dbEnv.force }).then(() => {
                const apollo = new ApolloServer({
                    schema,
                    context: ({ res, req }) => {
                        const user = auth.accessToken(res, req);
                        return {
                            models,
                            user,
                            res,
                        };
                    },
                    playground: {
                        settings: {
                            'editor.fontSize': 12,
                            'editor.theme': 'dark',
                            'request.credentials': 'include',
                        },
                    },
                    introspection: true,
                });

                apollo.applyMiddleware({
                    app: this.serverApp,
                    path: '/',
                    cors: {
                        credentials: true,
                        origin: ['http://localhost:3000'],
                        methods: ['GET', 'POST', 'PUT', 'DELETE'],
                    },
                });

                this.serverApp.listen(
                    {
                        port: process.env.GRAPHQL_PORT,
                    },
                    () => {
                        Logger.info(`Graphql server created successfully..`);
                        Logger.info(
                            `${process.env.GRAPHQL_ENDPOINT_URL}:${process.env.GRAPHQL_PORT}${process.env.GRAPHQL_PATH}`,
                        );
                    },
                );
            });
        } catch (err) {
            this.setErrorBoundaryVariables(err, 'S005', "Couldn't set a graphql server.");
        }
    }

    setServerHeartbeat() {
        Logger.info('Application heartbeat creating...');
        try {
            this.serverApp.get('/ping', (_req, res) => {
                res.end('pong');
            });
        } catch (err) {
            this.setErrorBoundaryVariables(err, 'S006', "Server hearbeat couldn't started.");
        }
    }
}

export default new Setup();
