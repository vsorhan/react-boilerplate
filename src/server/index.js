import Setup from '@server.setup';

Setup.createServerApp();

Setup.setGraphqlServer();

Setup.setWebpackDevServer();

Setup.setServerHeartbeat();
