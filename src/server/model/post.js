export default (sequelize, DataTypes) => {
    const Post = sequelize.define(
        'post',
        {
            id: {
                allowNull: false,
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
            },
            title: {
                type: DataTypes.STRING,
            },
            description: {
                type: DataTypes.TEXT,
            },
        },
        {
            freezeTableName: true,
            tableName: 'posts',
        },
    );

    Post.associate = models => {
        Post.belongsTo(models.User, {
            foreignKey: 'owner',
        });
    };

    return Post;
};
