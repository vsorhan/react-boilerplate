export default (sequelize, DataTypes) => {
    const User = sequelize.define(
        'user',
        {
            id: {
                allowNull: false,
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4,
            },
            username: {
                type: DataTypes.STRING,
                unique: true,
            },
            email: {
                type: DataTypes.STRING,
                unique: true,
            },
            password: {
                type: DataTypes.STRING,
            },
            firstname: {
                type: DataTypes.STRING,
            },
            lastname: {
                type: DataTypes.STRING,
            },
            phone: {
                type: DataTypes.STRING,
                unique: true,
            },
            address: {
                type: DataTypes.TEXT,
            },
        },
        {
            freezeTableName: true,
            tableName: 'users',
        },
    );

    User.associate = models => {
        User.hasMany(models.Post, {
            foreignKey: 'owner',
        });
    };

    return User;
};
