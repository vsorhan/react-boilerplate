import Sequelize from 'sequelize';
import { sequelize } from '@server.services';

const models = {
    User: sequelize.import('User', require('./user')),
    Post: sequelize.import('Post', require('./post')),
};

Object.keys(models).forEach(modelName => {
    if ('associate' in models[modelName]) {
        models[modelName].associate(models);
    }
});

models.sequelize = sequelize;
models.Sequelize = Sequelize;

export default models;
