import express from 'express';

const expressRouter = express.Router();

expressRouter.get('*', (req, res, next) => {
    console.log('ROUTER', req, res);
});

export default expressRouter;
