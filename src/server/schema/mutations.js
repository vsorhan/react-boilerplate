// Imports
import { GraphQLObjectType } from 'graphql';

// App Imports
import * as user from '@server.schema.user.mutations';
import * as post from '@server.schema.post.mutations';

// Mutation
const mutation = new GraphQLObjectType({
    name: 'Mutations',
    description: 'Here is the mutations',
    fields: {
        ...user,
        ...post,
    },
});

export default mutation;
