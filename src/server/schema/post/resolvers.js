import { AuthenticationError } from 'apollo-server-core';

// Get Post by ID
export async function getPost(parentValue, { id }, { models }) {
    return await models.Post.findOne({ where: { owner: user.id, id } });
}

// Get All Posts
export async function getAll(parent, args, { user, models }) {
    if (!user) throw new AuthenticationError('You are not authorizated');
    return await models.Post.findAll({
        include: [
            {
                model: models.User,
                where: {
                    id: user.id,
                },
                paranoid: false,
            },
        ],
    });
}

// Create Post
export async function create(parentValue, { ...otherArgs }, { user, models }) {
    try {
        if (!user) throw new AuthenticationError('You are not authorizated');
        return await models.Post.create({
            ...otherArgs,
            owner: user.id,
        });
    } catch (err) {
        console.log(err);
        throw new Error('Invalid User', err);
    }
}

// Delete Post
export async function remove(parentValue, { id }, { user, models }) {
    try {
        if (!user) throw new AuthenticationError('You are not authorizated');
        return await models.Post.destroy({ where: { id } });
    } catch (err) {
        throw new Error('Invalid User');
    }
}
