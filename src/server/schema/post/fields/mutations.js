import { GraphQLString, GraphQLInt } from 'graphql';

import { PostType } from '@server.schema.post.types';
import { create, remove, login } from '@server.schema.post.resolvers';

// Post create
export const postCreate = {
    type: PostType,
    args: {
        title: {
            name: 'title',
            type: GraphQLString,
        },
        description: {
            name: 'description',
            type: GraphQLString,
        },
    },
    resolve: create,
};

// User remove
export const postRemove = {
    type: PostType,
    args: {
        id: {
            name: 'id',
            type: GraphQLString,
        },
    },
    resolve: remove,
};
