import { GraphQLList } from 'graphql';

import { PostType } from '@server.schema.post.types';
import { getAll, getPost } from '@server.schema.post.resolvers';

// Posts All
export const posts = {
    type: new GraphQLList(PostType),
    resolve: getAll,
};

// Post By ID
export const post = {
    type: PostType,
    resolve: getPost,
};
