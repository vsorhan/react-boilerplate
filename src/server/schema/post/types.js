import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql';

const PostType = new GraphQLObjectType({
    name: 'Post',
    description: '...',
    fields: () => ({
        id: { type: GraphQLString },
        title: { type: GraphQLString },
        description: { type: GraphQLString },
    }),
});

export { PostType };
