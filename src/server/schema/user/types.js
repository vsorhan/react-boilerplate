import { GraphQLObjectType, GraphQLString, GraphQLInt } from 'graphql';

const UserType = new GraphQLObjectType({
    name: 'User',
    description: '...',
    fields: () => ({
        id: { type: GraphQLString },
        username: { type: GraphQLString },
        email: { type: GraphQLString },
        firstname: { type: GraphQLString },
        lastname: { type: GraphQLString },
        role: { type: GraphQLString },
    }),
});

const AuthType = new GraphQLObjectType({
    name: 'Auth',
    description: '...',
    fields: () => ({
        email: { type: GraphQLString },
        firstname: { type: GraphQLString },
        lastname: { type: GraphQLString },
        role: { type: GraphQLString },
    }),
});

const LoginType = new GraphQLObjectType({
    name: 'Login',
    description: '...',
    fields: () => ({
        email: { type: GraphQLString },
        firstname: { type: GraphQLString },
        lastname: { type: GraphQLString },
    }),
});

export { UserType, LoginType, AuthType };
