import { GraphQLString, GraphQLList } from 'graphql';

import { UserType, AuthType } from '@server.schema.user.types';
import { getAll, getUser, authUser } from '@server.schema.user.resolvers';

// Users All
export const users = {
    type: new GraphQLList(UserType),
    resolve: getAll,
};

// User By ID
export const user = {
    type: UserType,
    resolve: getUser,
};

// User By ID
export const authenticateUser = {
    type: AuthType,
    resolve: authUser,
};
