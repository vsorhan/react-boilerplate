import { GraphQLString, GraphQLInt } from 'graphql';

import { LoginType, UserType } from '@server.schema.user.types';
import { create, remove, login } from '@server.schema.user.resolvers';

// User create
export const userCreate = {
    type: UserType,
    args: {
        username: {
            name: 'username',
            type: GraphQLString,
        },
        email: {
            name: 'email',
            type: GraphQLString,
        },
        password: {
            name: 'password',
            type: GraphQLString,
        },
        firstname: {
            name: 'firstname',
            type: GraphQLString,
        },
        lastname: {
            name: 'lastname',
            type: GraphQLString,
        },
        role: {
            name: 'role',
            type: GraphQLString,
        },
    },
    resolve: create,
};

// User remove
export const userRemove = {
    type: UserType,
    args: {
        id: {
            name: 'id',
            type: GraphQLString,
        },
    },
    resolve: remove,
};

// User login
export const userLogin = {
    type: LoginType,
    args: {
        email: {
            name: 'email',
            type: GraphQLString,
        },
        password: {
            name: 'password',
            type: GraphQLString,
        },
    },
    resolve: login,
};
