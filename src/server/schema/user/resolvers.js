import bcrypt from 'bcryptjs';
import { AuthenticationError } from 'apollo-server-core';
import auth from '@server.services.auth';

// Get User by ID
export async function getById(parentValue, { id }, { user, models }) {
    if (!user) {
        throw new Error('You are not authorizated');
    }
    return await models.User.findOne({ where: { id } });
}

// Get User by ID
export async function getUser(parentValue, args, { user, models }) {
    if (!user) {
        throw new Error('You are not authorizated');
    }
    return await models.User.findOne({ where: { id: user.id } });
}

// Get All Users
export async function getAll(parent, args, { user, models }) {
    if (!user) {
        throw new Error('You are not authorizated', user);
    }
    return await models.User.findAll();
}

// Create User
export async function create(parentValue, { password, ...otherArgs }, { models }) {
    try {
        const hashedPassword = await bcrypt.hash(password, 12);
        return await models.User.create({
            ...otherArgs,
            password: hashedPassword,
        });
    } catch (err) {
        console.log(err);
        throw new Error('Invalid User', err);
    }
}

// Delete User
export async function remove(parentValue, { id }, { models }) {
    try {
        return await models.User.destroy({ where: { id } });
    } catch (err) {
        throw new Error('Invalid User');
    }
}

// Login User
export async function login(parentValue, { email, password }, { res, models }) {
    try {
        const user = await models.User.findOne({ where: { email } });

        if (!user) {
            throw new Error('No user with that email');
        }

        const valid = await bcrypt.compare(password, user.password);

        if (!valid) {
            throw new Error('Incorrect password');
        }

        const accessToken = auth.createAccessToken({
            id: user.id,
            email: user.email,
        });

        const refreshToken = auth.createRefreshToken(accessToken);

        res.cookie('access_token', accessToken, {
            httpOnly: true,
            secure: process.env.NODE_ENV === 'production',
            maxAge: 1000 * 60 * 10,
        });

        res.cookie('refresh_token', refreshToken, {
            httpOnly: true,
            secure: process.env.NODE_ENV === 'production',
            maxAge: 1000 * 60 * 60 * 24 * 1,
        });

        return user;
    } catch (err) {
        throw new Error('Invalid User', err);
    }
}

// Delete User
export async function authUser(parentValue, args, { user, models }) {
    try {
        return await models.User.findOne({ where: { id: user.id } });
    } catch (err) {
        throw new AuthenticationError('You are not authorizated.');
    }
}
