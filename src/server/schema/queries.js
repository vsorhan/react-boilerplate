// Imports
import { GraphQLObjectType } from 'graphql';

// App Imports
import * as user from '@server.schema.user.queries';
import * as post from '@server.schema.post.queries';

// Query
const query = new GraphQLObjectType({
    name: 'Queries',
    description: 'Here is the queries.',
    fields: {
        ...user,
        ...post,
    },
});

export default query;
