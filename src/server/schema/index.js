import { GraphQLSchema } from 'graphql';
import queries from '@server.schema.queries';
import mutations from '@server.schema.mutations';

// Schema
const schema = new GraphQLSchema({
    query: queries,
    mutation: mutations,
});

export default schema;
