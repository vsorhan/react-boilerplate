module.exports = {
    environment: process.env.NODE_ENV || 'development',
    defaultPort: 3000,
    assetPath: 'dist',
    viewEnginePath: 'dist/views',
    cors: {
        whiteList: ['http://localhost:3000'],
    },
    database: {
        development: {
            username: 'samedorhan',
            password: 'So.159742.',
            database: 'boilerplate',
            host: '127.0.0.1',
            dialect: 'postgres',
            force: false,
        },
    },
};
