import { createLogger, format, transports } from 'winston';
import morgan from 'morgan';
import moment from 'moment';

const Logger = class Logger {
    constructor() {
        this.logger = createLogger({
            format: format.combine(
                format.colorize(),
                format.timestamp(),
                format.splat(),
                format.printf(info => {
                    const { timestamp, level, message } = info;
                    const ts = moment(timestamp).format('MMM DD YYYY HH:mm:ss');
                    return ` ${ts} [${level}]: ${message}`;
                }),
            ),
            transports: [new transports.Console()],
        });
        this.logger.stream = {
            write: (message, encoding) => {
                this.logger.error(message);
            },
        };

        this.middleware = morgan('short', {
            skip: (req, res) => {
                return res.statusCode < 400;
            },
            stream: this.logger.stream,
        });
    }

    log(level, message) {
        this.logger.log({ level, message });
    }

    error(message) {
        this.logger.error({ level: 'error', message });
    }

    warn(message) {
        this.logger.warn({ level: 'warn', message });
    }

    verbose(message) {
        this.logger.verbose({ level: 'verbose', message });
    }

    info(message) {
        this.logger.info({ level: 'info', message });
    }

    debug(message) {
        this.logger.debug({ level: 'debug', message });
    }

    silly(message) {
        this.logger.silly({ level: 'silly', message });
    }
};

export default new Logger();
