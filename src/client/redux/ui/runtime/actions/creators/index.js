import * as Types from '@ui.runtime.actions.types';

export const createDialog = payload => ({
    type: Types.CREATE_NEW_DIALOG,
    payload,
});

export const removeDialog = payload => ({
    type: Types.REMOVE_DIALOG,
    payload,
});
