import { createReducer } from 'redux-immutablejs';
import Immutable, { List } from 'immutable';
import * as Types from '@ui.runtime.actions.types';

const initial = Immutable.fromJS({
    dialogs: List(),
});

const viewElements = createReducer(initial, {
    [Types.CREATE_NEW_DIALOG]: (state, action) =>
        state.mergeDeep({
            dialogs: [...state.get('dialogs'), action.payload],
        }),
    [Types.REMOVE_DIALOG]: (state, action) =>
        state.merge({
            dialogs: [...state.get('dialogs').filter(o => o !== action.payload)],
        }),
});

export default viewElements;
