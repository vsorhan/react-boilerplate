import { combineReducers } from 'redux';

import runtime from '@ui.runtime.reducer';

export default () =>
    combineReducers({
        runtime,
    });
