import { combineReducers } from 'redux-immutable';
import { connectRouter } from 'connected-react-router/immutable';
import domain from '@domain.reducers';
import ui from '@ui.reducers';

export default history =>
    combineReducers({
        domain,
        ui,
        router: connectRouter(history),
    });
