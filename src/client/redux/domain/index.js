import React from 'react';
import * as domainServices from '@domain.services';

export default function domain(Component, requestedDomains) {
    return class InjectedComponent extends React.Component {
        constructor(props) {
            super(props);
        }

        createDomain(domain, dispatch, props) {
            const dispatchMethod = dispatch ? this.props.dispatch || {} : {};
            const d = new domainServices[domain](props, this.props, dispatchMethod);
            return d;
        }

        injectDomains() {
            if (requestedDomains) {
                const domains = {};
                Object.keys(requestedDomains).forEach(domain => {
                    const currentDomain = requestedDomains[domain];
                    if (domainServices[domain]) {
                        domains[domain.toLowerCase()] = this.createDomain(
                            domain,
                            currentDomain.dispatch || true,
                            currentDomain.props || {},
                        );
                    }
                });
                return domains;
            }
            return {};
        }

        render() {
            const domains = this.injectDomains();
            return <Component {...this.props} domains={domains} />;
        }
    };
}
