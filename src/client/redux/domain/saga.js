import { all } from 'redux-saga/effects';

import authDomainAsyncInterface from '@domain.auth.saga';
import postDomainAsyncInterface from '@domain.post.saga';

export default function* rootSaga() {
    yield all([authDomainAsyncInterface(), postDomainAsyncInterface()]);
}
