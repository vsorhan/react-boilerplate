import { combineReducers } from 'redux-immutable';

import auth from '@domain.auth.reducer';
import post from '@domain.post.reducer';

export default combineReducers({
    auth,
    post,
});
