import * as gql from 'gql-query-builder';
import { push } from 'connected-react-router';
import { put, takeLatest, all, call, select } from 'redux-saga/effects';
import * as Types from '@domain.auth.actions.types';
import api from '@api';

class AuthAsyncInterface {
    constructor() {
        this.name = 'AuthAsyncInterface';
    }

    *authUser() {
        try {
            const authResponse = yield call(api.fetch, {
                type: 'query',
                operation: 'authenticateUser',
                fields: ['email', 'firstname', 'lastname'],
            });

            if (authResponse.data.authenticateUser) {
                yield all([
                    put({
                        type: Types.UPDATE_USER,
                        payload: authResponse.data.authenticateUser,
                    }),
                ]);
            } else {
                yield all([
                    put({
                        type: Types.LOGOUT_USER,
                    }),
                    // put(push('/sign-in'))
                ]);
            }
        } catch (err) {
            yield all([
                put({
                    type: Types.LOGOUT_USER,
                }),
                put(push('/sign-in')),
            ]);
        }
    }

    *login(action) {
        try {
            const loginResponse = yield call(api.fetch, {
                type: 'mutation',
                operation: 'userLogin',
                fields: ['email', 'firstname', 'lastname'],
                variables: {
                    email: action.payload.email,
                    password: action.payload.password,
                },
            });

            if (loginResponse.data.userLogin) {
                yield all([
                    put({
                        type: Types.UPDATE_USER,
                        payload: loginResponse.data.userLogin,
                    }),
                    put(push('/')),
                ]);
            }
        } catch (err) {
            console.log(err);
            yield put({
                type: Types.ERROR,
                payload: {
                    result: false,
                    err,
                },
            });
        }
    }
}

const authSagaInterface = new AuthAsyncInterface();
export default function* authDomainAsyncInterface() {
    yield all([
        takeLatest(Types.LOGIN_USER, authSagaInterface.login),
        takeLatest(Types.AUTHENTICATE_USER, authSagaInterface.authUser),
    ]);
}
