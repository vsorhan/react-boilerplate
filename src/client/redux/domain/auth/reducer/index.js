/* ----------------------------------- */
// Domain import pattern. Please       //
// follow below usage for all reducers //
/* ----------------------------------- */

import { createReducer } from 'redux-immutablejs';
import Immutable from 'immutable';
import initialState from '@domain.auth.reducer.initial';
import * as Types from '@domain.auth.actions.types';

const initial = Immutable.fromJS(initialState);

const auth = createReducer(initial, {
    [Types.LOGOUT_USER]: (state, action) =>
        state.mergeDeep({
            email: null,
            firstname: null,
            lastname: null,
            isLoggedIn: false,
        }),
    [Types.UPDATE_USER]: (state, action) =>
        state.mergeDeep({
            email: action.payload.email,
            firstname: action.payload.firstname,
            lastname: action.payload.lastname,
            isLoggedIn: true,
        }),
});

export default auth;
