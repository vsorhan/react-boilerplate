import { fromJS } from 'immutable';

export default fromJS({
    email: null,
    firstname: null,
    lastname: null,
    isLoggedIn: true,
});
