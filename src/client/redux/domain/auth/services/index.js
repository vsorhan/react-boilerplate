import * as Actions from '@domain.auth.actions.creators';

class Auth {
    constructor(props, componentProps, dispatch) {
        this.name = 'Auth';
        this.props = props;
        this.dispatch = dispatch;
        this.componentProps = componentProps;
        if (props && props.onMount) {
            this.onMount();
        }
    }

    onMount() {
        this.props.onMount();
    }

    authUser() {
        this.dispatch(Actions.authUser());
    }

    login(email, password) {
        this.dispatch(
            Actions.login({
                email,
                password,
            }),
        );
    }

    logout() {
        this.dispatch(Actions.logout());
    }
}

export default Auth;
