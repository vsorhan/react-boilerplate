export const LOGIN_USER = 'LOGIN_USER';
export const LOGOUT_USER = 'LOGOUT_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const AUTHENTICATE_USER = 'AUTHENTICATE_USER';
export const ERROR = 'ERROR';
