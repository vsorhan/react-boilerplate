import * as Types from '@domain.auth.actions.types';

export const authUser = () => ({
    type: Types.AUTHENTICATE_USER,
});

export const login = payload => ({
    type: Types.LOGIN_USER,
    payload,
});

export const logout = payload => ({
    type: Types.LOGOUT_USER,
});
