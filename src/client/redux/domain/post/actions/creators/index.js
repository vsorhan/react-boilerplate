import * as Types from '@domain.post.actions.types';

export const getPosts = payload => ({
    type: Types.LOAD_POST_LIST,
    payload,
});
