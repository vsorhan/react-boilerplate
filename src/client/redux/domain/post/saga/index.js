import { push } from 'connected-react-router';
import { put, takeLatest, all, call, select } from 'redux-saga/effects';
import api from '@api';
import * as Types from '@domain.post.actions.types';

class PostAsyncInterface {
    constructor() {
        this.name = 'PostAsyncInterface';
    }

    *list() {
        try {
            const listResponse = yield call(api.fetch, {
                type: 'query',
                operation: 'posts',
                fields: ['id', 'title', 'description'],
            });

            if (listResponse.data.posts) {
                yield put({
                    type: Types.UPDATE_POST_LIST,
                    payload: listResponse.data.posts,
                });
            }
        } catch (err) {
            console.log(err);
            yield put({
                type: Types.ERROR,
                payload: {
                    result: false,
                    err,
                },
            });
        }
    }
}

const postSagaInterface = new PostAsyncInterface();
export default function* postDomainAsyncInterface() {
    yield all([takeLatest(Types.LOAD_POST_LIST, postSagaInterface.list)]);
}
