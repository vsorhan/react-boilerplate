import * as Actions from '@domain.post.actions.creators';

class Auth {
    constructor(props, componentProps, dispatch) {
        this.name = 'Auth';
        this.props = props;
        this.dispatch = dispatch;
        this.componentProps = componentProps;
        if (props && props.onMount) {
            this.onMount();
        }
    }

    onMount() {
        this.props.onMount();
    }

    getPosts() {
        this.dispatch(Actions.getPosts());
    }
}

export default Auth;
