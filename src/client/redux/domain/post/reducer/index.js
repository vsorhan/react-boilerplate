/* ----------------------------------- */
// Domain import pattern. Please       //
// follow below usage for all reducers //
/* ----------------------------------- */

import { createReducer } from 'redux-immutablejs';
import Immutable from 'immutable';
import initialState from '@domain.post.reducer.initial';
import * as Types from '@domain.post.actions.types';

const initial = Immutable.fromJS(initialState);

const post = createReducer(initial, {
    [Types.UPDATE_POST_LIST]: (state, action) =>
        state.mergeDeep({
            list: action.payload,
        }),
});

export default post;
