import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import Immutable from 'immutable';

import { routerMiddleware } from 'connected-react-router/immutable';
import { createBrowserHistory } from 'history';
import combinedReducers from '@redux.reducers';
import rootSaga from '@domain.saga';

export default () => {
    const history = createBrowserHistory();

    const sagaMiddleware = createSagaMiddleware();
    const routeMiddleware = routerMiddleware(history);

    const middlewares = [sagaMiddleware, routeMiddleware];

    const composed = [applyMiddleware(...middlewares)];

    if (process.env.NODE_ENV === 'development') {
        // Redux dev tools activated for development environment.
        // eslint-disable-next-line
        composed.push(window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());
    }
    const store = createStore(combinedReducers(history), Immutable.Map({}), compose(...composed));

    sagaMiddleware.run(rootSaga);

    return {
        store,
        history,
    };
};
