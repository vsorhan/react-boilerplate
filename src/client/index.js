import React from 'react';
import ReactDOM from 'react-dom';

import { Provider } from 'react-redux';

import ReduxInstaller from '@redux';

import AppRouter from '@router';

import GlobalStyle from '@styles';

const { store, history } = ReduxInstaller('client');

ReactDOM.render(
  <Provider store={store}>
      <AppRouter history={history} />
      <GlobalStyle />
    </Provider>,
    document.getElementById('app'),
);
