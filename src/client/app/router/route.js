import React, { Component } from 'react';
import { Route } from 'react-router';
import Fader from '@fader';

class NormalRoute extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { key, path, Layout, Component } = this.props;
        return (
          <Route
              {...this.props}
              render={props => (
              <Layout>
                        <Fader _key={key} path={path}>
                            <Component {...props} />
                </Fader>
                    </Layout>
                )}
            />
        );
    }
}

export default NormalRoute;
