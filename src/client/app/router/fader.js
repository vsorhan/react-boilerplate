import React from 'react';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import styled from 'styled-components';

const transitionName = 'sc-Hjcxl';

const Animation = styled(TransitionGroup)`
    .${transitionName}-appear, .${transitionName}-enter, .${transitionName}-exit-active {
        opacity: 0;
        transform: translate3d(0px, -4px, 0px);
        transition: all 625ms ease;
    }

    .${transitionName}-appear-active, .${transitionName}-enter-active {
        opacity: 1;
        transform: translate3d(0px, 0px, 0px);
        transition: all 625ms ease;
    }
`;

const Fader = props => {
    const { children, path, key } = props;

    return (
      <Animation>
          <CSSTransition key={key} classNames={transitionName} timeout={1000} appear>
              {children}
            </CSSTransition>
        </Animation>
    );
};

export default Fader;
