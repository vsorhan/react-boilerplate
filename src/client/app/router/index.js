import React, { Component } from 'react';
import { ConnectedRouter } from 'connected-react-router/immutable';
import ApplicationRouter from '@router.router';

class AppRouter extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { history } = this.props;
        return (
          <ConnectedRouter history={history}>
              <ApplicationRouter />
            </ConnectedRouter>
        );
    }
}

export default AppRouter;
