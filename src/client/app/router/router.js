import React, { Component } from 'react';
import { Switch } from 'react-router';
import PrivateRoute from '@router.private';
import NormalRoute from '@router.route';
import routes from '@router.routes';

class ApplicationRouter extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        return (
          <Switch>
              {routes.map(({ Layout, Component, redirect, perm, ...prop }) => {
                    if (redirect)
                        return <RedirectRoute from={prop.path} to={prop.pathTo} key={prop.name} />;
                    if (perm)
                        return (
                          <PrivateRoute
                              key={prop.key}
                              exact={prop.exact}
                              path={prop.path}
                              title={prop.title}
                                Layout={Layout}
                              Component={Component}
                            />
                        );

                    return (
                      <NormalRoute
                          key={prop.key}
                            exact={prop.exact}
                          path={prop.path}
                          title={prop.title}
                            Layout={Layout}
                          Component={Component}
                        />
                    );
                })}
            </Switch>
        );
    }
}

export default ApplicationRouter;
