import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Route, Redirect } from 'react-router';
import domain from '@domain';
import Fader from '@fader';

class PrivateRoute extends Component {
    constructor(props) {
        super(props);
        this.state = {};

        this.auth = props.domains.auth;
    }

    componentDidMount() {
        this.auth.authUser();
    }

    render() {
        const { key, path, isAuth, Layout, Component } = this.props;
        return (
          <Route
              {...this.props}
              render={props =>
                    isAuth ? (
                      <Layout>
                          <Fader _key={key} path={path}>
                          <Component {...props} />
                        </Fader>
                        </Layout>
                    ) : (
                      <Redirect
                          to={{
                                pathname: '/sign-in',
                            }}
                        />
                    )
                }
            />
        );
    }
}

const mapDispatchToProps = dispatch => ({ dispatch });

const mapStateToProps = state => ({
    isAuth: state.getIn(['domain', 'auth', 'isLoggedIn']),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(
    domain(PrivateRoute, {
        Auth: {
            dispatch: true,
        },
    }),
);
