import * as Views from '@views';
import { Default, Launch } from '@layouts';

export default [
    {
        key: 'home',
        path: '/',
        exact: true,
        title: 'Home',
        perm: true,
        Layout: Default,
        Component: Views.Home,
    },
    {
        key: 'about',
        path: '/about',
        title: 'About',
        perm: true,
        Layout: Default,
        Component: Views.About,
    },
    {
        key: 'login',
        path: '/sign-in',
        title: 'Login',
        perm: false,
        Layout: Launch,
        Component: Views.Login,
    },
    {
        key: '404',
        title: '404 Not Found',
        perm: false,
        Layout: Default,
        Component: Views.NotFound,
    },
];
