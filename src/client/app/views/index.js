import About from '@views.about';
import Home from '@views.home';
import Login from '@views.login';
import NotFound from '@views.not-found';

export { About, Home, Login, NotFound };
