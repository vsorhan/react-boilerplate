import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import domain from '@domain';

class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        };

        this.post = this.props.domains.post;
    }

    componentDidMount() {
        this.post.getPosts();
    }

    render() {
        const { posts } = this.props;
        return (
          <div>
              <Link to="/about">About</Link>
              <h2>Home</h2>
                <ul>
              {posts.map(p => (
                      <li key={p.id}>
                            <h5>{p.title}</h5>
                            <p>{p.description}</p>
                        </li>
                    ))}
            </ul>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({ dispatch });

const mapStateToProps = state => ({
    posts: state.getIn(['domain', 'post', 'list']).toJS(),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(
    domain(Home, {
        Post: {
            dispatch: true,
        },
    }),
);
