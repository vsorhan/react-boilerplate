import React, { Component } from 'react';
import { connect } from 'react-redux';
import domain from '@domain';

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
        };

        this.auth = this.props.domains.auth;
    }

    changeModel(e) {
        e.persist();
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    login() {
        const { email, password } = this.state;
        this.auth.login(email, password);
    }

    render() {
        const { email, password } = this.state;
        return (
          <div>
              <h2>Login</h2>
              <input type="text" name="email" onChange={e => this.changeModel(e)} value={email} />
              <input
              type="password"
              name="password"
              onChange={e => this.changeModel(e)}
              value={password}
            />
              <button onClick={() => this.login()}>Login</button>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({ dispatch });

const mapStateToProps = state => ({});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(
    domain(Login, {
        Auth: {
            dispatch: true,
        },
    }),
);
