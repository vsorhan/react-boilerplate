import React, { Component } from 'react';

import { Main, Content } from '@layouts.default.comps';

class Launch extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { children } = this.props;
        return (
          <Main>
              <Content>{children}</Content>
            </Main>
        );
    }
}

export default Launch;
