import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Main, Content } from '@layouts.default.comps';

class Default extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {
        const { children } = this.props;
        return (
            <Main>
                <Content>{children}</Content>
          </Main>
        );
    }
}

Default.propTypes = {
    children: PropTypes.element.isRequired,
};

export default Default;
