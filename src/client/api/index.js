import axios from 'axios';
import * as gql from 'gql-query-builder';

class API {
    constructor() {
        this.name = 'API';
    }

    fetch({ type, operation, fields, variables }) {
        return new Promise((resolve, reject) => {
            axios({
                method: 'POST',
                url: `${process.env.GRAPHQL_ENDPOINT_URL}:${process.env.GRAPHQL_PORT}${
                    process.env.GRAPHQL_PATH
                }`,
                data: gql[type]({
                    operation,
                    fields,
                    variables,
                }),
                withCredentials: true,
            })
                .then(response => {
                    resolve(response.data);
                })
                .catch(err => {
                    reject(err);
                });
        });
    }
}

export default new API();
