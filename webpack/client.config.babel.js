
import { resolve, join } from 'path';

import dotenv from 'dotenv';

import webpack from 'webpack';

import HtmlWebpackPlugin from 'html-webpack-plugin';
import CopyWebpackPlugin from 'copy-webpack-plugin';

module.exports = (env, argv) => {
    const config = {
        mode: argv.mode,
        entry : {
            app: './src/client/index.js',
            vendor: [
                'react',
                'react-dom'
            ]
        },
        output: {
            path : resolve(__dirname, '..', 'dist'),
            filename: argv.mode === 'development' ? "[name].bundle.js" : "[name].bundle.[hash:6].js",
        },
        devServer: {
            host: 'localhost',
            port: 3000,
        },
        module: {
            rules: [
                {
                    test: /\.(js|jsx)$/,
                    exclude: /(node_modules|bower_components)/,
                    loader: 'babel-loader'
                },
                {
                    test: /\.(png|jpg|gif|svg|eot|ttf|woff|woff2)$/,
                    loader: 'url-loader',
                }
            ]
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: '!!raw-loader!public/index.html',
                filename: 'index.html',
                minify: {
                    removeComments: true,
                    collapseWhitespace: true,
                    conservativeCollapse: true
                }
            }),
            new CopyWebpackPlugin([
                {
                from: 'public/assets/images/',
                to: 'assets/images/[name].[ext]',
                test: /\.(png|jpg|gif|svg)$/
                },
            ]),
            new CopyWebpackPlugin([
                {
                from: 'public/assets/fonts/',
                to: 'assets/fonts/[name].[ext]',
                test: /\.(ttf|woff|woff2)$/
                },
            ]),
            new webpack.HotModuleReplacementPlugin()
        ]
    };

    if(argv.mode === 'development'){
        const envFile = dotenv.config().parsed;
        if(envFile) {
            const envKeys = Object
                            .keys(envFile)
                            .reduce((prev, next) => {
                                prev[`process.env.${next}`] = JSON.stringify(envFile[next]);
                                return prev;
                            }, {});
            config.plugins.push( new webpack.DefinePlugin(envKeys) );
        }
    }

    return config;
}