NODE                = node
NODE_MODULES        = node_modules
YARN                = yarn
YARN_LOCK           = yarn.lock
WEBPACK             = webpack
WEBPACK_DEV_SERVER  = webpack-dev-server
NODEMON             = nodemon --watch false
DOCKER              = docker
DOCKER_COMPOSE      = docker-compose
BABEL_NODE          = babel-node -e
ENV_CMD             = env-cmd
ENV                 = .env

WEBPACK_DEV_OPTS    = --mode development --open --devtool inline-source-map --verbose  --display-entrypoints
WEBPACK_PROD_OPTS   = --mode production -p --verbose --display-entrypoint
WEBPACK_CONF_CLIENT = --config webpack/client.config.babel.js
WEBPACK_CONF_SERVER = --config webpack/server.config.babel.js

WEBPACK_DEV_CLIENT  = --watch $(WEBPACK_DEV_OPTS) $(WEBPACK_CONF_CLIENT) --color --hot

SERVER_ENTRY_POINT  = src/server/index.js

V = 0
Q = $(if $(filter 1,$V),,@)
M = $(shell printf "\033[34;1m▶\033[0m")

V = 0
Q = $(if $(filter 1,$V),,@)
M = $(shell printf "\033[34;1m▶\033[0m")

# Misc
clean: ; $(info $(M) CLIENT -> Old build cleaning...)    @ ## Cleanup everything
	$(Q) @rm -rf dist/*

uninstall: clean ; $(info $(M) CLIENT -> Node modules removing...)    @ ## Removing node modules folder
	$(Q) @rm -rf $(NODE_MODULES) $(YARN_LOCK)

install: clean ; $(info $(M) CLIENT -> Application initializing...)    @
	$(Q) @rm -rf $(NODE_MODULES) $(YARN_LOCK) && $(YARN) install --silent && $(YARN) global add --silent webpack webpack-cli

# Development build
build: ; $(info $(M) UI => Development client building...)    @
	$(Q) $(WEBPACK) $(WEBPACK_DEV_OPTS) $(WEBPACK_CONF_CLIENT)

# Starts development server
start: ; $(info $(M) UI => Development server starting...)    @
	$(Q) $(ENV_CMD) -f $(ENV) $(NODEMON) $(SERVER_ENTRY_POINT) --exec $(BABEL_NODE)
